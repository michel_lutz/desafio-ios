//
//  LoadItemTableViewCell.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 01/11/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import UIKit

/**
 Class LoadItemTableViewCell
 - TableViewCellPara Load de novos itens da table
 
 */
class LoadItemTableViewCell: UITableViewCell {
    ///Define outlet de UIActivityIndicatorView
    @IBOutlet weak var loadActivity: UIActivityIndicatorView!
    /**
     :nodoc:
     */
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    /**
     :nodoc:
     */
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
