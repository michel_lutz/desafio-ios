//
//  RepositoryTableViewCell.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 01/11/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import UIKit
import SDWebImage

/**
 Class RepositoryTableViewCell
 
 - TableViewCell Para Resposiórios.
 */
class RepositoryTableViewCell: UITableViewCell {
    ///Nome do repositório
    @IBOutlet weak var lblName: UILabel!
    ///Descrição do repositório
    @IBOutlet weak var lblDescription: UILabel!
    ///username dono do repositório
    @IBOutlet weak var lblUsername: UILabel!
    ///Total de forks do repositório
    @IBOutlet weak var lblForks: UILabel!
    ///Total de Stars do repositório
    @IBOutlet weak var lblStars: UILabel!
    ///Avatar dono do repositório
    @IBOutlet weak var imgAuthor: UIImageView!

    /**
     Método para configuração de celula, recebe model e preenche labels
     
     - Parameter repository: `Repository`
     */
    func configureCell(with repository: Repository){
        lblName.text = repository.name
        lblDescription.text = repository.description
        lblUsername.text = repository.owner.login
        lblForks.text = String(repository.total_forks)
        lblStars.text = String(repository.total_stars)
        imgAuthor.sd_setImage(with: URL(string: repository.owner.avatar)!, completed: nil)
        imgAuthor.layer.cornerRadius = imgAuthor.frame.size.width / 2
        imgAuthor.layer.masksToBounds = true
        imgAuthor.layer.borderWidth = 1
        imgAuthor.layer.borderColor = UIColor.lightGray.cgColor
    }
}
