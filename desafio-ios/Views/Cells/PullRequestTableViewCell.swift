//
//  PullRequestTableViewCell.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 03/11/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import UIKit
import SDWebImage
/**
 Class PullRequestTableViewCell
 
 - TableViewCell Para PullRequests de Repositórios.
 */
class PullRequestTableViewCell: UITableViewCell {
    ///Título do pull
    @IBOutlet weak var lblTitulo: UILabel!
    ///body do pull
    @IBOutlet weak var lblBody: UILabel!
    ///Avatar do pull
    @IBOutlet weak var avatar: UIImageView!
    ///Username do pull
    @IBOutlet weak var lblUsername: UILabel!
    ///Data do pull
    @IBOutlet weak var lblData: UILabel!
    /**
     Método para configuração de celula, recebe model e preenche labels
     
     - Parameter pull: `Pull`
     */
    func configureCell(with pull: Pull){
        lblTitulo.text = pull.title
        lblData.text = pull.date.formataBR()
        lblBody.text = pull.body
        avatar.sd_setImage(with: URL(string: pull.owner.avatar)!, completed: nil)
        avatar.layer.cornerRadius = avatar.frame.size.width / 2
        avatar.layer.masksToBounds = true
        avatar.layer.borderWidth = 1
        avatar.layer.borderColor = UIColor.lightGray.cgColor
        lblUsername.text = pull.owner.login
    }
}
