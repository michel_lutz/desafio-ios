//
//  RepositoryTableViewController.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 01/11/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import UIKit
import ObjectMapper

/**
 RepositoryTableViewController Table View controller para repositórios
 */
class RepositoryTableViewController: UITableViewController {
    ///RequestPage resquestPage
    let resquestPage = RequestPage()
    //Page? page
    var page: Page?
    //[Repository] repositories
    var repositories: [Repository] = []
    //Int resultCount
    var resultCount = 0
    //Int page_counter
    var page_counter = 1
    //Repository? selectedRepository
    var selectedRepository: Repository?
    ///UIActivityIndicatorView activityIndicator
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    /**
     :nodoc:
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        confifureNib()
        loadData(page: page_counter)
        if resultCount == 0{
            initActivityIndicator()
        }
    }
    /**
     :nodoc:
     */
    fileprivate func confifureNib(){
        let nibName = UINib(nibName: "RepositoryTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "repositoryCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 136.0
    }
    
    // MARK: Activity Inticator
    /**
     Configura initActivityIndicator
     */
    func initActivityIndicator(){
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    // MARK: - Load Data
    /**
     Carrega dados da Api atualiza itens da tabela e recarrega table
     */
    private func loadData(page: Int){
        resquestPage.getDataPage(page: String(page_counter)){ (response) in
            switch response {
            case .success(let model):
                self.page = model
                self.repositories.append(contentsOf: model.items)
                self.resultCount += model.items.count
                self.tableView.reloadData()
                self.activityIndicator.stopAnimating()
            case .serverError(let description):
                print("Server error: \(description) \n")
            case .noConnection(let description):
                print("Server error noConnection: \(description) \n")
            case .timeOut(let description):
                print("Server error timeOut: \(description) \n")
            }
        }
    }
    
    // MARK: - Table view data source
    /**
     :nodoc:
     */
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.resultCount
    }
    /**
     :nodoc:
     */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == resultCount {
            let cellLoad = Bundle.main.loadNibNamed("LoadItemTableViewCell", owner: self, options: nil)?.first as! LoadItemTableViewCell
            //Inicio a animação
            cellLoad.loadActivity.startAnimating()
            return cellLoad
        }
        //
        let cell = tableView.dequeueReusableCell(withIdentifier: "repositoryCell", for: indexPath) as! RepositoryTableViewCell
        cell.configureCell(with: repositories[indexPath.row])
        return cell
    }
    /**
     Altura padrão da celula
     
     - Returns: 136.0 Float altura da celula
     */
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 136.0
    }
    /**
     :nodoc:
     */
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        print("display row: \(indexPath.row)")
        if indexPath.row == (resultCount - 1) {
            page_counter += 1
            loadData(page: page_counter)
        }
    }
    /**
     :nodoc:
     */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRepository = self.repositories[indexPath.row]
        self.performSegue(withIdentifier: "pullRequestSegue", sender: nil)
    }
    
    // MARK: - Navigation
    /**
     :nodoc:
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pullRequestSegue"{
            let view = segue.destination as! PullRequestTableViewController
            view.selectedRepository = self.selectedRepository
        }
    }
}
