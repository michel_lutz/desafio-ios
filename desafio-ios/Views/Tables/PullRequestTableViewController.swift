//
//  PullRequestTableViewController.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 02/11/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import UIKit
/**
 PullRequestTableViewController Table View controller para Pull Requests
 */
class PullRequestTableViewController: UITableViewController {
    ///Repository? selectedRepository
    var selectedRepository: Repository?
    ///RequestPull requestPull
    let requestPull = RequestPull()
    ///[Pull] pulls
    var pulls: [Pull] = []
    ///UIActivityIndicatorView activityIndicator
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    /**
     :nodoc:
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        if pulls.count == 0{
            self.initActivityIndicator()
        }
    }
    /**
     :nodoc:
     */
    fileprivate func confifureNib(){
        let nibName = UINib(nibName: "PullRequestTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "pullRecCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 136.0
    }
    
    // MARK: Activity Inticator
    /**
     Configura initActivityIndicator
     */
    func initActivityIndicator(){
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    // MARK: Activity Inticator
    /**
     Alert func para exibição de avisos
     
     Parameters:
        - message: Mensagem do Alert
        - title: Título do Alert
     */
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - LoadData
    /**
     Carrega dados da Api atualiza itens da tabela e recarrega table
     */
    private func loadData(){
        requestPull.getData(user: (selectedRepository?.owner.login)!, repositoryName: (selectedRepository?.name)!){ (response) in
            switch response {
            case .success(let model):
                self.pulls = model
                if model.count == 0{
                    self.alert(message: "Nenhum Pull Request encontrado.", title: "Pull Requests")
                }
                self.tableView.reloadData()
                self.activityIndicator.stopAnimating()
            case .serverError(let description):
                print("Server error: \(description) \n")
            case .noConnection(let description):
                print("Server error noConnection: \(description) \n")
            case .timeOut(let description):
                print("Server error timeOut: \(description) \n")
            }
        }
    }
    // MARK: - Table view data source
    /**
     :nodoc:
     */
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pulls.count
    }
    /**
     Altura padrão da celula
     
     - Returns: 170,00 Float altura da celula
     */
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    /**
     :nodoc:
     */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pullRecCell", for: indexPath) as! PullRequestTableViewCell
        
        cell.configureCell(with: pulls[indexPath.row])
        return cell
    }
    /**
     :nodoc:
     */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        open(link: pulls[indexPath.row].url)
    }
    /**
     Abre Url de Pullrequest, verifica versão do ios para chamada especifica
     
     - Parameter link: String com link de pullrequest
     */
    func open(link: String) {
        if let url = URL(string: link) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                })
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
