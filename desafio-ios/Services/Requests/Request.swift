//
//  Request.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 31/10/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation
import Alamofire

/**
 Class Request
 - Configura SessionManager para requisições a API do GitHub
 */
class Request{
    /**
     alamofireManager configura SessionManager para requisições
     
     - Returns: `SessionManager`
     */
    let alamofireManager: SessionManager = {
        //Criação das configurações
        let configurarion = URLSessionConfiguration.default
        //Tempo de timeout em milesegundos
        configurarion.timeoutIntervalForRequest = 10000
        configurarion.timeoutIntervalForResource = 10000
        return SessionManager(configuration: configurarion)
    }()
}
