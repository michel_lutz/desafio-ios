//
//  Responses.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 31/10/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation

/**
 Response Page

 - success:  Page
 - serverError: ServerError
 - timeOut: ServerError
 - noConnection: ServerError
 */
enum ResponsePage{
    ///success: Page
    case success(model: Page)
    ///serverError: ServerError
    case serverError(description: ServerError)
    ///timeOut: ServerError
    case timeOut(desciption: ServerError)
    ///noConnection: ServerError
    case noConnection(description: ServerError)
}

/**
 Response Pull
 
 - success:  [Pull]
 - serverError: ServerError
 - timeOut: ServerError
 - noConnection: ServerError
 */
enum ResponsePull{
    ///success:  [Pull]
    case success(model: [Pull])
    ///serverError: ServerError
    case serverError(description: ServerError)
    ///timeOut: ServerError
    case timeOut(desciption: ServerError)
    ///noConnection: ServerError
    case noConnection(description: ServerError)
}
