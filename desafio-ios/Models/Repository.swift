//
//  Repository.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 31/10/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 Repository extends `Mappable`

 - SeeAlso: `Mappable`
 */
struct Repository: Mappable{
    ///String name
    var name: String
    ///String description
    var description: String
    ///Owner owner
    var owner: Owner
    ///Int total_stars
    var total_stars: Int
    ///Int total_forks
    var total_forks: Int
    /**
     - Parameter map: `Map`
     */
    init?(map: Map) {
        name = (try? map.value("name")) ?? ""
        description = (try? map.value("name")) ?? ""
        owner = (try? map.value("owner")) ?? Owner(map: map)!
        total_stars = (try? map.value("stargazers_count")) ?? 0
        total_forks = (try? map.value("forks_count")) ?? 0
    }
    /**
     - Parameter map: `Map`
     */
    mutating func mapping(map: Map) {
        name <- map[name]
        description <- map["description"]
        owner <- map["owner"]
        total_stars <- map["stargazers_count"]
        total_forks <- map["forks_count"]
    }
}
