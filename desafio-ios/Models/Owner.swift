//
//  Owner.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 02/11/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 Owner extends `Mappable`
 
 - SeeAlso: `Mappable`
 */
struct Owner: Mappable {
    ///String login
    var login: String
    ///String avatar
    var avatar: String
    
    /**
     - Parameter map: `Map`
     */
    init?(map: Map) {
        login = (try? map.value("login")) ?? ""
        avatar = (try? map.value("avatar_url")) ?? ""
    }
    /**
     - Parameter map: `Map`
     */
    mutating func mapping(map: Map) {
        login <- map["login"]
        avatar <- map["avatar_url"]
    }
}
