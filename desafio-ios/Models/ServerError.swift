//
//  ServerError.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 31/10/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation

/**
 struct ServerError guarda informação de erro de servidor e status code do mesmo
 */
struct ServerError{
    ///String msgError
    let msgError: String
    ///Int statusCode
    let statusCode: Int
}
