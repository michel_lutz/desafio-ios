//
//  Pull.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 31/10/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 Pull extends `Mappable`
 - SeeAlso: `Mappable`
 */
struct Pull: Mappable{
    ///String title
    var title: String
    ///String body
    var body: String
    ///Owner owner
    var owner: Owner
    ///Date date
    var date: Date
    ///String url
    var url: String
    /**
     - Parameter map: `Map`
     */
    init?(map: Map) {
        title = (try? map.value("title")) ?? ""
        body = (try? map.value("body")) ?? ""
        owner = (try? map.value("user")) ?? Owner(map: map)!
        date = (try? map.value("created_at")) ?? Date()
        url = (try? map.value("html_url")) ?? ""
    }
    /**
     - Parameter map: `Map`
     */
    mutating func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        owner <- map["user"]
        date <- map["created_at"]
        url <- map["html_url"]
    }
}
