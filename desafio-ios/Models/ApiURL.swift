//
//  ApiURL.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 31/10/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation

/**
    Struct ApiURL
 */
struct ApiURL {
    /// - String base
    static let base: String = "https://api.github.com/"
    /// - String search
    static let search: String = ApiURL.base + "search/repositories?"
    /// - String repo
    static let repo: String = ApiURL.base + "repos/"
    /// - String java
    static let java: String = ApiURL.search + "q=language:Swift&sort=stars&page="
}
