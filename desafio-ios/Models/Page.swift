//
//  Page.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 02/11/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 Page extends `Mappable`
 
 - SeeAlso: `Mappable`
 */
struct Page: Mappable{
    ///Int total_count
    var total_count: Int
    //[Repository] items
    var items: [Repository]
    /**
     - Parameter map: `Map`
     */
    init?(map: Map) {
        total_count = (try? map.value("total_count")) ?? 0
        items = [(try? map.value("items")) ?? Repository(map: map)!]
    }
    /**
     - Parameter map: `Map`
     */
    mutating func mapping(map: Map) {
        total_count <- map["total_count"]
        items <- map["items"]
    }
}
