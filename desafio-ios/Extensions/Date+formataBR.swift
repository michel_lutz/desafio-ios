//
//  Date+formataBR.swift
//  desafio-ios
//
//  Created by Michel Anderson Lutz Teixeira on 03/11/17.
//  Copyright © 2017 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation

/**
 Extension para date
 */
extension Date{
    /**
     func formataBR Formata data em 00/00/0000 dia/mês/ano
     
     :SeeAlso: `DateFormatter`
     :returns: String Data formatada
     */
    func formataBR() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: self)
    }
}
